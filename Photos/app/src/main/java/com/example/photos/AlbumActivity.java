package com.example.photos;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

public class AlbumActivity extends AppCompatActivity {
    public Admin storer = MainActivity.adminStorer;
    int index;
    private ListView listView;
    ArrayList<String> photosNames = new ArrayList<>();
    ImageView image;
    Intent myFileIntent;
    TextView message;
    Button add;
    int SELECT_PHOTO = 0;
    Uri uri;
    int number = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        message = (TextView) findViewById(R.id.message);

        image = (ImageView) findViewById(R.id.Photos);

        add = (Button) findViewById(R.id.button4);

        Bundle bundle = getIntent().getExtras();

        index = (int) bundle.getInt("Index");

        ArrayList<Photo> temp = storer.getUser("username").getAlbums().get(index).list;
        for(int i = 0;i < temp.size();i++){
            photosNames.add(temp.get(i).name);
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album, photosNames);

        listView = findViewById(R.id.photos_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view, position, id) -> showPhotos(position));


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent,SELECT_PHOTO);
//                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                intent.addCategory(Intent.CATEGORY_TYPED_OPENABLE);
//                intent.setType("image/*");
//                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null && data.getData() != null){
            uri = data.getData();
            //String path = data.getData().getPath();
//            String[] projection = { MediaStore.Images.Media.DATA };
//            Cursor cursor = managedQuery(uri, projection, null, null, null);
//            startManagingCursor(cursor);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            String path = cursor.getString(column_index);
            String path = uri.toString();
            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                image.setImageBitmap(bitmap);
                File file = new File(path);
                message.setText(path);
                //!storer.getUser("username").getAlbums().get(index).not_exist(path)
                if(true) {
                    Photo temp = new Photo(path,file);
                    temp.uri = uri;
                    storer.getUser("username").getAlbums().get(index).list.add(new Photo(path, file));
                    //storer.getUser("username").getAlbums().get(index).list.add(new Photo("/storage/emulated/0/Download/unnamed.jpg", new File("/storage/emulated/0/Download/unnamed.jpg")));
                    //storer.getUser("username").getAlbums().get(index).list.add(new Photo("/document/image:29", new File("/document/image:29")));
                    //Admin.storeAdmin(storer);
                    message.setText("Photo is Added Successfully!");
                    Admin.storeAdmin(storer);
                }else{
                    message.setText("existed!");
                }
            }catch (FileNotFoundException e){
                e.printStackTrace();
            } catch(IOException e){
                e.printStackTrace();
            }

            refresh();

        }

    }


    public void Open_Click(View v){
        if(number != -1){
            Bundle bundle = new Bundle();
            bundle.putInt("Index_Album", index);
            bundle.putInt("Index_Photo", number);
            Intent intent = new Intent(this, PhotoActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);}
        else{
            message.setText("Plz, select a photo in list");
        }
    }

    public void Delete_Click(View v) throws IOException{
        if(number == -1){
            message.setText("Plz, select a photo in list");
        }else{
            storer.getUser("username").getAlbums().get(index).list.remove(number);
            Admin.storeAdmin(storer);
            number = -1;
            refresh();
        }
        //refresh();
    }

    public void Slide_Click(View v){
        if(storer.getUser("username").getAlbums().get(index).list.size() == 0) {
            message.setText("No Picture. Cannot enter into slide mode");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Index_Album", index);
        bundle.putInt("Index_Photo", number);
        Intent intent = new Intent(this, SlideActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void refresh(){
        //storer.getUser("username").getAlbums().get(index).list.append(new Photo());
        photosNames.clear();
        ArrayList<Photo> temp = storer.getUser("username").getAlbums().get(index).list;
        for(int i =0;i<temp.size();i++){
            photosNames.add(temp.get(i).name);
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album, photosNames);

        listView = findViewById(R.id.photos_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view, position, id) -> showPhotos(position));
    }

    public void showPhotos(int position) {
        //image.setImageResource(storer.getUser("username").getAlbums().get(index).list.get(position).name);
        number = position;
        message.setText("The index of selected item is :" + Integer.toString(position));
        Uri uri2 = Uri.parse(storer.getUser("username").getAlbums().get(index).list.get(position).name);
        image.setImageURI(uri2);
        //File temp = storer.getUser("username").getAlbums().get(index).list.get(position).file;
        //File test = new File(storer.getUser("username").getAlbums().get(index).list.get(position).filepath);
        //Image tem = new Image(temp.toURI().toString());
        //Uri uri1 = storer.getUser("username").getAlbums().get(index).list.get(position).uri;
        //image.setImageURI(uri1);
        //Bitmap bitmap = BitmapFactory.decodeFile(storer.getUser("username").getAlbums().get(index).list.get(position).name);
//        try {
//
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri1);
//                image.setImageBitmap(bitmap);
//
//        }catch (FileNotFoundException e){
//                e.printStackTrace();
//            } catch(IOException e){
//                e.printStackTrace();
//            }

//        Uri uri1 = Uri.parse(temp.getAbsolutePath());
//        try {
//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri1);
//            image.setImageBitmap(bitmap);
//        }catch (FileNotFoundException e){
//            e.printStackTrace();
//        } catch(IOException e){
//            e.printStackTrace();
//        }
//        image.setImageURI(uri1);
        //message.setText(storer.getUser("username").getAlbums().get(index).list.get(position).filepath);

    }
}