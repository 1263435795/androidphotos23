package com.example.photos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Bohui xia
 * @author Shumin Jiang
 */
public class Admin implements Serializable {

    /**
     * serial version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * user list
     */
    public ArrayList<User> users;
    /**
     * Constructor of the Admin class
     */
    public Admin() {
        users = new ArrayList<User>();
        users.add(new User("admin", "admin"));
    }
    /**
     * get all the users for Admin account
     * @return ArrayList of User
     */
    public ArrayList<User> getUsers(){
        return users;
    }
    /**
     * get password by given username
     * @param temp username
     * @return password
     */
    public String getPassword(String temp) {
        for(User user:users) {
            if(user.username.equals(temp)) {
                return user.password;
            }
        }
        return null;
    }
    /**
     * find the user by username
     * @param temp username of user
     * @return targeted user
     */
    public User getUser(String temp) {
        for(User user : users) {
            if(user.username.equals(temp)) {
                return user;
            }
        }
        return null;
    }
    /**
     * add a user to the list
     * @param temp targeted user
     * @return the index of added user
     */
    public int addUser(User temp) {
        int index = 0;
        //users.add(new User(temp.username, temp.password));
        users.add(temp);
        index = users.indexOf(temp);
        return index;
    }
    /**
     * delete a user in the list
     * @param index index number of targeted user
     */
    public void deleteUser(int index) {
        users.remove(index);
    }
    /**
     * check user
     * @param name username
     * @param Password pssword
     * @return true if existed
     */
    public boolean checkUser(String name, String Password) {
        for(int i = 0; i < users.size(); i++) {
            if((users.get(i).username.equals(name))&&(users.get(i).password.equals(Password))) {
                return true;
            }
        }
        return false;
    }
    /**
     * get the index of targeted user
     * @param name username of target
     * @return index of target
     */
    public int getUserIndex(String name) {
        int index = 0;
        for(int i = 0; i < users.size(); i++) {
            if(users.get(i).username.equals(name)) {
                index = i;
            }
        }
        return index;
    }
    /**
     * Save's state to .dat file
     * @param store the admin need to be stored
     * @throws IOException to throw IO Exception
     */
    public static void storeAdmin(Admin store) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/data/data/com.example.photos/files/data.dat"));
        oos.writeObject(store);
        oos.close();
    }

    /**
     * Loads from dat file
     * @return userlist
     * @throws IOException to throw IO Exception
     * @throws ClassNotFoundException to throw Class Not Found Exception
     */
    public static Admin getAdmin() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/data/data/com.example.photos/files/data.dat"));
        Admin userList = (Admin) ois.readObject();
        ois.close();
        return userList;
    }

}