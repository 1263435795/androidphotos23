package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    public static Admin adminStorer = new Admin();
    String addAlbum = "";
    //Button Add;
    EditText albumInput;
    private ListView listView;
    // items in the list
    ArrayList<String> albumNames = new ArrayList<>();
    User user = new User("username","password");
    //Log.("myTag", "This is my message");
    //private static final String TAG = "mytag";
    int check = adminStorer.addUser(user);
    File file = new File("/data/data/com.example.photos/files/data.dat");
    TextView message;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list);

        try {
            adminStorer = Admin.getAdmin();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(!file.exists()){
            Context context = this;
            File f = new File(context.getFilesDir(), "data.dat");
            try {
                f.createNewFile();
            }
            catch (IOException e){

            }
        }
        //albumNames = getResources().getStringArray(R.array.albums_array);
        message = (TextView) findViewById(R.id.here);

        ArrayList<Album> temp = adminStorer.getUser("username").getAlbums();
        for(int i = 0;i < temp.size();i++){
            albumNames.add(temp.get(i).albumname);
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album, albumNames);

        listView = findViewById(R.id.albums_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view, position, id) -> showAlbum(position));

        //albumInput = (EditText) findViewById(R.id.Album_name);
        //Add = (Button) findViewById(R.id.Add);
        /*
        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlbum = albumInput.getText().toString();
                if(addAlbum.equals("") || addAlbum.isEmpty()){

                }else{
                    ad.getUser("username").getAlbums().add(new Album(addAlbum));
                    refresh();
                }
            }
        });
        */
    }

    public void Add_Click(View v) throws IOException {
        albumInput = findViewById(R.id.name_here);
        addAlbum = albumInput.getText().toString();
        if(addAlbum.equals("") || addAlbum.isEmpty()){
            message.setText("can not input null");
            //ad.getUser("username").getAlbums().add(new Album(addAlbum));
        } else if(adminStorer.getUser("username").alreadyHave(addAlbum)){
            message.setText("Same name has already existed.");
        }
        else{
            adminStorer.getUser("username").getAlbums().add(new Album(addAlbum));

        }
        refresh();
        Admin.storeAdmin(adminStorer);
    }

    private void refresh(){
        albumNames.clear();
        ArrayList<Album> temp = adminStorer.getUser("username").getAlbums();
        for(int i = 0;i < temp.size();i++){
            albumNames.add(temp.get(i).albumname);
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album, albumNames);

        listView = findViewById(R.id.albums_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view, position, id) -> showAlbum(position));
    }

    private void showAlbum(int pos) {
        Bundle bundle = new Bundle();
        //bundle.putString(ROUTE_NAME, routeNames[pos]);
        //bundle.putString(ROUTE_DETAIL, routeDetails[pos]);
        bundle.putInt("Index", pos);
        bundle.putSerializable("User", adminStorer.getUser("username"));
        Intent intent = new Intent(this, ShowAlbum.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void Search_Click(View v){
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}