package com.example.photos;

import android.net.Uri;

import java.io.File;
import java.io.Serializable;
import java.util.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;

/**
 *
 * @author Bohui Xia
 * @author Shumin Jiang
 */
public class Photo implements Serializable {
    /**
     * serial version uid
     */
    private static final long serialVersionUID = 1L;

    //public static final String storeDir = "dat";
    //public static final String storeFile = "photos.dat";
    public ArrayList<String> location;
    public ArrayList<String> person;
    /**
     * photo name
     */
    public String name;
    /**
     * image file
     */
    public File file;
    public Uri uri;
    /**
     * tag list
     */
    public ArrayList<Tag> list;
    /**
     * caption
     */
    public String caption;
    /**
     * file path
     */
    public String filepath;
    /**
     * calendar
     */
    public Calendar calendar;
    /**
     * date of photo
     */
    public Date date;
    /**
     * Constructor of the Photo class
     * @param name the name of photo
     * @param file this file of image
     */
    public Photo(String name,File file) {
        this.name = name;
        if(file != null) {
            this.file = new File(name);
        } else {
            this.file = file;
        }
        this.uri = null;
        this.list = new ArrayList<Tag>();
        calendar = new GregorianCalendar();
        calendar.set(Calendar.MILLISECOND, 0);
        this.date = calendar.getTime();
        this.location = new ArrayList<String>();
        this.person = new ArrayList<String>();
    }
    /**
     * get tag list from the photo
     * @return tag list
     */
    public ArrayList<Tag> getTags() {
        return list;
    }
    public ArrayList<String> getPersons() {
        return person;
    }
    public ArrayList<String> getLocations() {
        return location;
    }
    /**
     * add a tag to the photo
     * @param tag the name of tag
     * @param value the value of tag
     */
    public void addTag(String tag,String value) {
        list.add(new Tag(tag,value));
    }
    /**
     * delete a tag of the photo
     * @param tag the name of targeted tag
     * @param value the value of targeted tag
     */
    public void deleteTag(String tag,String value) {
        if(list.size() > 0) {
            for(int i = 0; i < list.size(); i++) {
                if(tag.equals(list.get(i).name)&&value.equals(list.get(i).value)) {
                    list.remove(i);
                }
            }
        }
    }
    /**
     * check if tag in this photo
     * @param tag name of tag
     * @param value value of tag
     * @return true if tag in this photo
     */
    public boolean checkExist(String tag,String value) {
        //System.out.println(list.size());
        if(list.size() > 0) {
            for(int i = 0; i < list.size(); i++) {
                if(tag.equals(list.get(i).name)&&value.equals(list.get(i).value)) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean isAndExist(String inputPerson,String inputLocation) {
        //System.out.println(list.size());
        if(person.size() > 0) {
            for(int i = 0; i < person.size(); i++) {
                int temp = person.get(i).indexOf(inputPerson);
                if(temp>=0) {
                    if(location.size() > 0) {
                        for(int j = 0; j < location.size(); j++) {
                            int tmp = location.get(j).indexOf(inputLocation);
                            if(tmp>=0) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    public boolean isOrExist(String inputPerson,String inputLocation) {
        //System.out.println(list.size());
        if(person.size() > 0) {
            for(int i = 0; i < person.size(); i++) {
                int tmp = person.get(i).indexOf(inputPerson);
                if(tmp>=0) {
                    return true;
                }
            }
        }
        if(location.size() > 0) {
            for(int j = 0; j < location.size(); j++) {
                int tmp = location.get(j).indexOf(inputLocation);
                if(tmp>=0) {
                    return true;
                }
            }
        }
        return false;
    }

}
