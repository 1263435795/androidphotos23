package com.example.photos;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Bohui Xia
 * @author Shumin Jiang
 */
public class User implements Serializable, Comparable<User> {

    /**
     * serial version uid
     */
    private static final long serialVersionUID = 1L;
    /**
     * path to store
     */
    public static final String storeDir = "src/main";
    /**
     * file to store
     */
    public static final String storeFile = "users.dat";
    /**
     * username of user
     */
    public String username;
    /**
     * password of user
     */
    public String password;
    /**
     * album list of user
     */
    public ArrayList<Album> list;

    /**
     * Constructor of the User class
     * @param username username of user account
     * @param password password of user account
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
        list = new ArrayList<Album>();
    }
    /**
     * count the number of album
     * @return the number of album
     */
    public int count() {
        return list.size();
    }
    /**
     * delete an album in user
     * @param index the index of targeted album
     */
    public void deleteAlbum(int index) {
        list.remove(index);
    }
    /**
     * check if new album name is same to a existed album name
     * @param name new album name
     * @return true if new album name has existed in list
     */
    public boolean alreadyHave(String name) {
        for(Album album : list) {
            if (album.albumname.equals(name)) {
                return true;
            }
        }
        return false;
    }
    /**
     * get the album by index in list
     * @param index index of album
     * @return album whose index is equal to input
     */
    public Album getAlbum_index(int index) {
        return list.get(index);
    }
    /**
     * implement Comparable
     * @param u a user
     * @return result of comparing
     */
    public int compareTo(User u) {
        int x = this.username.compareToIgnoreCase(u.username);
        return x;
    }
    /**
     * get album list from user
     * @return album list
     */
    public ArrayList<Album> getAlbums() {
        return list;
    }
    /**
     * get album by provided name of album
     * @param Album_name album name
     * @return targeted album
     */
    public Album getAlbum_name(String Album_name) {
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i).albumname.equals(Album_name)) {
                return list.get(i);
            }
        }
        return null;
    }
    /**
     * add album to list
     * @param album the album need to be added
     */
    public int addAlbum(Album album) {
        list.add(album);
        return 0;
    }
    /**
     * get photo in the range of date
     * @param from from date
     * @param to to date
     * @return targeted photo list
     */
    public ArrayList<Photo> getPhotosresult(LocalDate from, LocalDate to){
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        start.set(from.getYear(), from.getMonthValue(), from.getDayOfMonth());
        end.set(to.getYear(), to.getMonthValue(), to.getDayOfMonth());
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                Date date = photo.date;
                Calendar temp = Calendar.getInstance();
                temp.setTime(date);
                Calendar photodate = Calendar.getInstance();
                photodate.set(temp.get(Calendar.YEAR), temp.get(Calendar.MONTH)+1, temp.get(Calendar.DAY_OF_MONTH));
                if(photodate.compareTo(start) > 0 && photodate.compareTo(end) < 0) {
                    result.add(photo);
                }else if(photodate.equals(start)){
                    result.add(photo);
                }else if(photodate.equals(end)){
                    result.add(photo);
                }

            }
        }
        return result;
    }
    /**
     * get photo by one tag
     * @param Gname tag name
     * @param Gvalue tag value
     * @return targeted photo list
     */
    public ArrayList<Photo> getPhotoByTag(String Gname, String Gvalue){
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                for(Tag tag : photo.getTags()) {
                    if((tag.name.equals(Gname))&&(tag.value.equals(Gvalue))) {
                        result.add(photo);
                    }
                }
            }
        }
        return result;
    }
    public ArrayList<Photo> getPhotoByPerson(String inputPerson) {
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                for(String str : photo.getPersons()) {
                    int tmp = str.indexOf(inputPerson);
                    if(tmp>=0) {
                        result.add(photo);
                    }
                }
            }
        }
        return result;
    }
    public ArrayList<Photo> getPhotoByLocation(String inputLocation) {
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                for(String str : photo.getLocations()) {
                    int tmp = str.indexOf(inputLocation);
                    if(tmp>=0) {
                        result.add(photo);
                    }
                }
            }
        }
        return result;
    }
    /**
     * find photo by two tag with AND relation
     * @param Gname1 tag1 name
     * @param Gvalue1 tag1 value
     * @param Gname2 tag2 name
     * @param Gvalue2 tag1 value
     * @return targeted photo list
     */
    public ArrayList<Photo> getPhotoByAnd(String Gname1, String Gvalue1, String Gname2, String Gvalue2){
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                if(photo.checkExist(Gname1, Gvalue1)&&photo.checkExist(Gname2, Gvalue2)) {
                    result.add(photo);
                }
            }
        }
        return result;
    }
    public ArrayList<Photo> getPhoto_And(String inputPerson, String inputLocation){
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                if(photo.isAndExist(inputPerson, inputLocation)) {
                    result.add(photo);
                }
            }
        }
        return result;
    }
    /**
     * find photo by two tag with OR relation
     * @param Gname1 tag1 name
     * @param Gvalue1 tag1 value
     * @param Gname2 tag2 name
     * @param Gvalue2 tag1 value
     * @return targeted photo list
     */
    public ArrayList<Photo> getPhotoByOr(String Gname1, String Gvalue1, String Gname2, String Gvalue2){
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                if(photo.checkExist(Gname1, Gvalue1)||photo.checkExist(Gname2, Gvalue2)) {
                    result.add(photo);
                }
            }
        }
        return result;
    }
    public ArrayList<Photo> getPhoto_Or(String inputPerson, String inputLocation){
        ArrayList<Photo> result = new ArrayList<Photo>();
        for(Album album : list) {
            for(Photo photo : album.getPhotos()) {
                if(photo.isOrExist(inputPerson, inputLocation)) {
                    result.add(photo);
                }
            }
        }
        return result;
    }

}
