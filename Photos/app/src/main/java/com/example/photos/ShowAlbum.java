package com.example.photos;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class ShowAlbum extends AppCompatActivity {
    public Admin storer = MainActivity.adminStorer;
    EditText rename_txt;
    String rename_str = "";
    TextView message;
    User us;
    int index;
    boolean check_delete = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        //String routeName = bundle.getString(Routes.ROUTE_NAME);
        us = (User) bundle.getSerializable("User");
        index = (int) bundle.getInt("Index");
        String name = us.getAlbums().get(index).albumname;

        TextView album_name = findViewById(R.id.album_name);

        message = (TextView) findViewById(R.id.message);

        album_name.setText(name);

        //message.setText("deleted Successfully, please click 'return'");
        /*
        Button delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                us.getAlbums().remove(index);
                message.setText("deleted Successfully, please click 'return'");
                finish();
            }
        });

        Button rename = (Button) findViewById(R.id.rename);
        delete.setOnClickListener(new View.OnClickListener(){
            public void onClick(View x){
                rename_txt = findViewById(R.id.rename_str);
                rename_str = rename_txt.getText().toString();
                if(rename_str.isEmpty()||rename_str.equals("")) {
                    us.getAlbums().get(index).albumname = rename_str;
                    message.setText("renamed Successfully, please click 'return'");
                    ActionBar actionBar = getActionBar();
                    actionBar.setDisplayHomeAsUpEnabled(true);
                }
            }
        });

         */

    }

    public void delete_Click(View v) throws IOException {
        if(check_delete) {
            storer.getUser("username").getAlbums().remove(index);
            Admin.storeAdmin(storer);
            message.setText("deleted Successfully, please click 'return'");
            check_delete = false;
            //ActionBar actionBar = getActionBar();
            //actionBar.setDisplayHomeAsUpEnabled(true);
        }else{
            message.setText("already deleted, check in the previous page");
        }
        //finish();
    }

    public void rename_Click(View v) throws IOException{
        rename_txt = findViewById(R.id.rename_str);
        rename_str = rename_txt.getText().toString();
        if(rename_str.isEmpty()||rename_str.equals("")) {
            message.setText("Input can not be null");
            //ActionBar actionBar = getActionBar();
            //actionBar.setDisplayHomeAsUpEnabled(true);
        }else{
            if(check_delete) {
                storer.getUser("username").getAlbums().get(index).albumname = rename_str;
                Admin.storeAdmin(storer);
                message.setText("renamed Successfully, please click 'return'");
            }else{
                message.setText("Album has already deleted");
            }
        }
    }

    public void show_Click(View v){
        if(check_delete) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("User", us);
            bundle.putInt("Index", index);
            Intent intent = new Intent(this, AlbumActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else{
            message.setText("Album has already deleted, Can not open");
        }
    }

}