package com.example.photos;

import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;

public class DisplayActivity extends AppCompatActivity {
    ImageView ph;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ph = (ImageView) findViewById(R.id.Photo);

        Bundle bundle = getIntent().getExtras();
        //String routeName = bundle.getString(Routes.ROUTE_NAME);
        String path = (String) bundle.getSerializable("path");

        Uri uri2 = Uri.parse(path);
        ph.setImageURI(uri2);
    }
}