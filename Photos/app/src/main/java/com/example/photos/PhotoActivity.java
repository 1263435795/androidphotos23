package com.example.photos;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class PhotoActivity extends AppCompatActivity {
    public Admin storer = MainActivity.adminStorer;
    int album;
    int photo;
    int PIndex = -1;
    int LIndex = -1;
    private ListView listViewP;
    private ListView listViewL;
    ArrayList<String> Person = new ArrayList<>();
    ArrayList<String> Location = new ArrayList<>();
    TextView photo_name;
    TextView message;
    EditText Input;
    boolean moved = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        //String routeName = bundle.getString(Routes.ROUTE_NAME);
        album = (int) bundle.getSerializable("Index_Album");
        photo = (int) bundle.getInt("Index_Photo");
        Input = (EditText)  findViewById(R.id.person_location);
        photo_name = (TextView) findViewById(R.id.name_photo);
        message = (TextView) findViewById(R.id.message);

        Photo temp = storer.getUser("username").getAlbums().get(album).list.get(photo);
        photo_name.setText(temp.name);

        refresh();

    }

    public void addP_Click(View v) throws IOException {
        if(moved){
            message.setText("Photo is moved, cannot edit");
            return;
        }
        Photo temp = storer.getUser("username").getAlbums().get(album).list.get(photo);
        String name = Input.getText().toString();
        if(name.equals("") || name.isEmpty()){
            message.setText("Please, input something");
        }else{
            if(temp.person.contains(name)){
                message.setText("Same Tag existed in Person list");
            } else {
                temp.person.add(name);
                Admin.storeAdmin(storer);
                refresh();
            }
        }
    }

    public void addL_Click(View v) throws IOException{
        if(moved){
            message.setText("Photo is moved, cannot edit");
            return;
        }
        Photo temp = storer.getUser("username").getAlbums().get(album).list.get(photo);
        String name = Input.getText().toString();
        if(name.equals("") || name.isEmpty()){
            message.setText("Please, input something");
        }else{
            if(temp.location.contains(name)){
                message.setText("Same Tag existed in Location list");
            } else {
                temp.location.add(name);
                Admin.storeAdmin(storer);
                refresh();
            }
        }
    }

    public void delete_Click(View v) throws IOException{
        if(moved){
            message.setText("Photo is moved, cannot edit");
            return;
        }
        Photo temp = storer.getUser("username").getAlbums().get(album).list.get(photo);
            if(PIndex == -1 && LIndex == -1){
                message.setText("Plz, select an item in the list");
            }
            else if(PIndex == -1){
                temp.location.remove(LIndex);
                message.setText("An location tag has been moved.");
                Admin.storeAdmin(storer);
                PIndex = -1;
                LIndex = -1;
                refresh();
            }
            else{
                temp.person.remove(PIndex);
                message.setText("An Person tag has been moved.");
                Admin.storeAdmin(storer);
                PIndex = -1;
                LIndex = -1;
                refresh();
            }
    }

    public void move_Click(View v) throws IOException{
        if(moved){
            message.setText("Photo is moved, cannot edit");
            return;
        }
        Photo temp = storer.getUser("username").getAlbums().get(album).list.get(photo);
        String name = Input.getText().toString();
        if(name.equals("") || name.isEmpty()){
            message.setText("Please, input something");
        }else{
            if(storer.getUser("username").alreadyHave(name)){
                storer.getUser("username").getAlbum_name(name).list.add(storer.getUser("username").getAlbums().get(album).list.get(photo));
                storer.getUser("username").getAlbums().get(album).list.remove(photo);
                message.setText("Successfully move, please return to home page");
                Admin.storeAdmin(storer);
                moved = true;
            }else{
                message.setText("No album name is same to input, try again");
            }
        }


    }

    public void display(View v){
        Bundle bundle = new Bundle();
        bundle.putString("path", storer.getUser("username").getAlbums().get(album).list.get(photo).name);
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void refresh(){
        ArrayList<String> tempP = storer.getUser("username").getAlbums().get(album).list.get(photo).person;
        for(int i = 0;i < tempP.size();i++){
            Person.add(tempP.get(i));
        }

        ArrayAdapter<String> adapterP =
                new ArrayAdapter<>(this, R.layout.album, tempP);

        ArrayList<String> tempL = storer.getUser("username").getAlbums().get(album).list.get(photo).location;
        for(int i = 0;i < tempP.size();i++){
            Location.add(tempP.get(i));
        }

        ArrayAdapter<String> adapterL =
                new ArrayAdapter<>(this, R.layout.album, tempL);

        listViewP = findViewById(R.id.Person_list);
        listViewL = findViewById(R.id.Location_list);
        listViewP.setAdapter(adapterP);
        listViewL.setAdapter(adapterL);
        listViewP.setOnItemClickListener((parent, view, position, id) -> showP(position));
        listViewL.setOnItemClickListener((parent, view, position, id) -> showL(position));
    }

    public void showP(int position){
        LIndex = -1;
        PIndex = position;
        message.setText("Selected a Person tag, index : " + Integer.toString(position));
    }

    public void showL(int position){
        PIndex = -1;
        LIndex = position;
        message.setText("Selected a Location tag, index : " + Integer.toString(position));
    }


}