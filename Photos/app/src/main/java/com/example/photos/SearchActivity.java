package com.example.photos;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {
    private ListView photolist;
    EditText inputP, inputL;
    String InputPerson = "";
    String InputLocation = "";
    TextView message;
    ArrayList<String> photoNames = new ArrayList<>();
    ArrayList<Photo> photoList = new ArrayList<>();
    public Admin storer = MainActivity.adminStorer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        message = (TextView) findViewById(R.id.mhere);

        //Bundle bundle = getIntent().getExtras();
    }

    public void Search_Person(View view) {
        message.setText("");
        photoList = new ArrayList<>();
        inputP = findViewById(R.id.person);
        InputPerson = inputP.getText().toString();
        if(InputPerson.equals("") || InputPerson.isEmpty()){
            message.setText("Please input Person");
            return;
            //ad.getUser("username").getAlbums().add(new Album(addAlbum));
        }else{
            photoList = storer.getUser("username").getPhotoByPerson(InputPerson);
            update();
        }
    }

    public void Search_Location(View view) {
        message.setText("");
        photoList = new ArrayList<>();
        inputL = findViewById(R.id.location);
        InputLocation = inputL.getText().toString();
        if(InputLocation.equals("") || InputLocation.isEmpty()){
            message.setText("Please input Location");
            return;
            //ad.getUser("username").getAlbums().add(new Album(addAlbum));
        }else{
            photoList = storer.getUser("username").getPhotoByLocation(InputLocation);
            update();
        }
    }

    public void Search_And(View view) {
        message.setText("");
        photoList = new ArrayList<>();
        inputP = findViewById(R.id.person);
        InputPerson = inputP.getText().toString();
        inputL = findViewById(R.id.location);
        InputLocation = inputL.getText().toString();
        if(InputPerson.equals("") || InputPerson.isEmpty()){

            if(InputLocation.equals("") || InputLocation.isEmpty()){
                message.setText("Please input Person and Location");
                return;
                //ad.getUser("username").getAlbums().add(new Album(addAlbum));
            }
            message.setText("Please input Person");
            return;
            //ad.getUser("username").getAlbums().add(new Album(addAlbum));
        }else{
            if(InputLocation.equals("") || InputLocation.isEmpty()){
                message.setText("Please input Location");
                return;
                //ad.getUser("username").getAlbums().add(new Album(addAlbum));
            }else{
                photoList = storer.getUser("username").getPhoto_And(InputPerson, InputLocation);
                update();
            }
        }
    }

    public void Search_Or(View view) {
        message.setText("");
        photoList = new ArrayList<>();
        inputP = findViewById(R.id.person);
        InputPerson = inputP.getText().toString();
        inputL = findViewById(R.id.location);
        InputLocation = inputL.getText().toString();
        if(InputPerson.equals("") || InputPerson.isEmpty()){

            if(InputLocation.equals("") || InputLocation.isEmpty()){
                message.setText("Please input Person and Location");
                return;
                //ad.getUser("username").getAlbums().add(new Album(addAlbum));
            }
            message.setText("Please input Person");
            return;
            //ad.getUser("username").getAlbums().add(new Album(addAlbum));
        }else{
            if(InputLocation.equals("") || InputLocation.isEmpty()){
                message.setText("Please input Location");
                return;
                //ad.getUser("username").getAlbums().add(new Album(addAlbum));
            }else{
                photoList = storer.getUser("username").getPhoto_Or(InputPerson, InputLocation);
                update();
            }
        }
    }
    private void update(){
        photoNames.clear();
        for(int i = 0;i < photoList.size();i++){
            photoNames.add(photoList.get(i).name);
        }


        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.album, photoNames);

        photolist = findViewById(R.id.photoslist);
        photolist.setAdapter(adapter);

        //photolist.setOnItemClickListener((parent, view, position, id) -> showPhoto(position));
    }

}