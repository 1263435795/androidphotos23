package com.example.photos;

import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SlideActivity extends AppCompatActivity {
    public Admin storer = MainActivity.adminStorer;
    int index = 0;
    ImageView image;
    TextView album_name;
    TextView message;
    Button last;
    Button next;
    int album,photo;
    ArrayList<Photo> plist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        last = (Button) findViewById(R.id.last);
        next = (Button) findViewById(R.id.next);
        message = (TextView) findViewById(R.id.message);
        album_name = (TextView) findViewById(R.id.album);
        image = (ImageView) findViewById(R.id.photo);

        Bundle bundle = getIntent().getExtras();
        album = (int) bundle.getSerializable("Index_Album");
        photo = (int) bundle.getInt("Index_Photo");

        album_name.setText(storer.getUser("username").getAlbums().get(album).albumname);
        plist = storer.getUser("username").getAlbums().get(album).list;

        update(0);
    }

    public void minus(View v){
        if(index == 0){
            message.setText("No Last Picture");
        }else{
            index--;
            update(index);
        }
    }

    public void add(View v){
        if(index == plist.size()-1){
            message.setText("No Next Picture");
        }else{
            index++;
            update(index);
        }
    }

    public void update(int position){
        Uri uri2 = Uri.parse(storer.getUser("username").getAlbums().get(album).list.get(position).name);
        image.setImageURI(uri2);
    }
}