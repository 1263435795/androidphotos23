package com.example.photos;

import java.io.Serializable;

/**
 *
 * @author Bohui Xia
 * @author Shumin Jiang
 */
public class Tag implements Serializable {
    /**
     * serial version uid
     */
    private static final long serialVersionUID = 1L;

    //public static final String storeDir = "dat";
    //public static final String storeFile = "tags.dat";
    /**
     * tag name
     */
    public String name;
    /**
     * tag value
     */
    public String value;
    /**
     * Constructor of the Tag class
     * @param name The name of tag
     * @param value The value of tag
     */
    public Tag(String name, String value) {
        this.name = name;
        this.value = value;
    }
}

