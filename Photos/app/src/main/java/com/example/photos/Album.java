package com.example.photos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Bohui Xia
 * @author Shumin Jiang
 */
public class Album implements Serializable {
    /**
     * serial version uid
     */
    private static final long serialVersionUID = 1L;
    /**
     * path to store
     */
    public static final String storeDir = "src/main";
    /**
     * file to store
     */
    public static final String storeFile = "users.dat";
    /**
     * album name
     */
    public String albumname;
    /**
     * photo list
     */
    public ArrayList<Photo> list;
    /**
     * Constructor of the Album class
     * @param name the name of new album
     */
    public Album(String name) {
        this.albumname = name;
        list = new ArrayList<Photo>();
    }
    /**
     * get the photo number in this album
     * @return photo number
     */
    public int count() {
        return list.size();
    }
    /**
     * get the list of photo in this album
     * @return list of photo
     */
    public ArrayList<Photo> getPhotos(){
        return list;
    }
    /**
     * check if photo in this album
     * @param photoname the name of targeted photo
     * @return true if in this album and false if not in this album
     */
    public boolean not_exist(String photoname) {
        for(int i = 0; i < list.size(); i++) {
            if(photoname.equals(list.get(i).name)) {
                return false;
            }
        }
        return true;
    }
    /**
     * get photo from this album
     * @param photoname the name of targeted photo
     * @return targeted photo in this album
     */
    public Photo getPhoto(String photoname) {
        for(int i = 0; i < list.size(); i++) {
            if(photoname.equals(list.get(i).name)) {
                return list.get(i);
            }
        }
        return null;
    }
    /**
     * delete a photo in this album
     * @param photoname the name of targeted photo
     */
    public void delete(String photoname) {
        for(int i = 0; i < list.size(); i++) {
            if(photoname.equals(list.get(i).name)) {
                list.remove(i);
            }
        }
    }
    /**
     * get the oldest date of photo in this album
     * @return oldest date
     */
    public String getFrom() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("E, M-d-y 'at' h:m:s a");
        Date date = null;
        String dateStr;
        if (list.isEmpty()) {
            return "NoDate";
        }
        date = this.getPhotos().get(0).date;
        for (Photo photo: list) {
            if (photo.date.before(date)) {
                date = photo.date;
            }
        }
        dateStr = dateFormatter.format(date);
        return dateStr;
    }
    /**
     * get the earliest date of photo in this album
     * @return earliest date
     */
    public String getTo() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("E, M-d-y 'at' h:m:s a");
        Date date = null;
        String dateStr;
        if (list.isEmpty()) {
            return "NoDate";
        }
        date = this.getPhotos().get(0).date;
        for (Photo photo: list) {
            if (photo.date.after(date)) {
                date = photo.date;
            }
        }
        dateStr = dateFormatter.format(date);
        return dateStr;
    }
}
