Shumin Jiang & Bohui Xia

sj677 & bx47

Several hint with our androidphotos23 project:

1.In all page, click on button back button will go back to the main page, sorry for any inconvience.
While user want to go back to the last page, user can click the back button on the phone to do it. However, during coding, we didn't consider it so using this button may cause unfixed bugs. 
If any bug appear, please delete all albums in main page and rewrite its. Hopefully, the app will run normally.

At least, we didn't meet bug in our app. 

![picture](img/MainActivity.png)

2.In Main page, user can write the new album name and then click add on button will do the adding activity. Click search will go to the search page. Clck on any album in the list can go to the album page.

![picture](img/Album.png)

3.In Album page, user can writhe the album name user want to rename and click on the buttom rename will do the rename activity, it will not update immediately, but when user click on back to go back to the main page, the album name will have be already changed.

4.In Album page, click on button delete will do the deleting activity, and when user click on back to go back to the main page, the album will have be already deleted in the list.

5.In Album page, user can click show photo list to go to the Album Activity page

![picture](img/AlbumActivity.png)

6.In Album Activity page, the ListView will show all the photos inside, and click on them can show the thumbnail images. After choosing one photo, user can click on button open to go to the Photo page and button delete to delete this photo.

7.In Album Activity page, click on button add will show the resource folder to let user choose photo to add. The name of photo will be set to the path of the image automatically. Thus, the name of photo always is very long.

8.In Album Activity page, user can click on button slide mode to go to the Slide Activity page. User can not enter into slide mode if your album doesn't contain any picture.

![picture](img/Photo.png)

9.In Photo page, user can write Person Tag or Location Tag and click on button add person/location to add tag to the chosen photo.

10.In Photo page, user can write other album's name and then click on the button move to other album to move the photo to other album.

11.In Photo page, user can click on delete to delete the chosen Tag.

12.In Photo page, user can click on the display button to see the full image of the chosen photo.

13.In Photo page, user can click on the "Move to other album" button to move picture to another album, the target album name also need to write into blank above "Add person" button. If photo is moved successfully, user can not do any operation in the page and need to return to main page.

![picture](img/Serach.png)

14.In Search page, user can input person tag and location tag and then click on any of the four button to do one of the four methods of searching activity.

![picture](img/SlideActivity.png)

15.In SlideActivity, Click "last picture" to show last picture and click "Next picture" to show next picture. User can not enter into this mode if your album doesn't contain any picture.

![picture](img/DisplayActivity.png)

15.In DisplayActivity, No button can be clicked and this page only will show the photo you choose.
